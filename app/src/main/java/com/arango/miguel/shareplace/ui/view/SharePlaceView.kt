package com.arango.miguel.shareplace.ui.view

interface SharePlaceView: BaseView {

    fun onAddPhotoReference(photoReference: String)

    fun onSharePlaceSuccess()
}