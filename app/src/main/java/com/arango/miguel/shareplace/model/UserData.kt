package com.arango.miguel.shareplace.model

data class UserData(
    val name: String = "",
    val phone: String = ""
)