package com.arango.miguel.shareplace.ui.view

import com.arango.miguel.shareplace.model.Place

interface HomeView: BaseView {

    fun showLoadingPlaces()
    fun dismissLoadingPlaces()

    fun renderPlaces(places: ArrayList<Place>)
}