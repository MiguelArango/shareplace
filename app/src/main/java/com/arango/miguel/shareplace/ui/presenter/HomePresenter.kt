package com.arango.miguel.shareplace.ui.presenter

import com.arango.miguel.shareplace.model.Place
import com.arango.miguel.shareplace.ui.view.HomeView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class HomePresenter: Presenter<HomeView>() {

    var firebaseDatabase = FirebaseFirestore.getInstance()
    private lateinit var auth: FirebaseAuth
    private lateinit var currentUser: FirebaseUser

    override fun initialize() {
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser!!
        getPlaces()
    }

    private fun getPlaces(){
        getView()?.showLoadingPlaces()

        val docRef = firebaseDatabase.collection(Place.COLLECTION_NAME)
        docRef.get().addOnCompleteListener{
                task ->
            if (task.isSuccessful) {
                getView()?.dismissLoadingPlaces()

                val querySnapshot = task.result

                if (querySnapshot != null) {
                    val places: ArrayList<Place> = ArrayList()
                    for (document in querySnapshot) {
                        val newPlace = document.toObject(Place::class.java)
                        places.add(newPlace)
                    }
                    getView()?.renderPlaces(places)
                } else {
                    //Log.d(HomeFragment.TAG, "No such document")
                }
            } else {
                getView()?.dismissLoadingPlaces()
            }
        }
    }
}