package com.arango.miguel.shareplace.ui.view

import android.content.Context
import com.google.firebase.storage.StorageReference

interface ProfileView: BaseView {

    fun getContext(): Context
    fun disableNameInput()
    fun enableNameInput()

    fun setInputName(name: String)

    fun renderProfilePhoto(pathReference: StorageReference)


}