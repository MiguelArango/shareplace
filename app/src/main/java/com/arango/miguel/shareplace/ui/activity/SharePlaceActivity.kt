package com.arango.miguel.shareplace.ui.activity

import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.arango.miguel.shareplace.R
import com.arango.miguel.shareplace.model.Place
import com.arango.miguel.shareplace.ui.presenter.SharePlacePresenter
import com.arango.miguel.shareplace.ui.view.SharePlaceView
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_share_place.*
import kotlinx.android.synthetic.main.fragment_home.bSharePlace
import java.io.File

class SharePlaceActivity : AppCompatActivity(), View.OnClickListener, SharePlaceView {

    override fun onSharePlaceSuccess() {
        finish()
    }

    override fun onAddPhotoReference(photoReference: String) {
        photosUrl.add(photoReference)
    }

    override fun onClick(v: View?) {
        val item_id = v?.id
        when (item_id) {
            R.id.bSharePlace -> {
                val description = tilDescription.editText?.text.toString()
                presenter?.sharePlace(description, photosUrl)
            }
            R.id.fabTakePhoto -> {
            }
            R.id.cvFirstPhoto -> {
                startImagePicker()
                photoPosition = 0
            }
            R.id.cvSecondPhoto -> {
                startImagePicker()
                photoPosition = 1
            }
        }
    }

    var photosUrl = ArrayList<String>()
    var photoPosition : Int = 0
    var presenter : SharePlacePresenter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_place)
        initPresenter()

        bSharePlace.setOnClickListener(this)
        fabTakePhoto.setOnClickListener(this)
        cvFirstPhoto.setOnClickListener(this)
        cvSecondPhoto.setOnClickListener(this)
    }

    private fun initPresenter(){
        presenter = SharePlacePresenter()
        presenter?.setView(this)
        presenter?.initialize()
    }


    private fun startImagePicker(){
        // You could also get the Intent
        ImagePicker.cameraOnly().start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            val image : Image = ImagePicker.getFirstImageOrNull(data)
            val myBitmap = BitmapFactory.decodeFile(image.path)

            if(photoPosition == 0){
                ivFirstPhoto.setImageBitmap(myBitmap)
            }   else{
                ivSecondPhoto.setImageBitmap(myBitmap)
            }
            presenter?.uploadPhoto(image.path)

        }

        super.onActivityResult(requestCode, resultCode, data)
    }





}
