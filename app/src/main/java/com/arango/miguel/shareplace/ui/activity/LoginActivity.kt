package com.arango.miguel.shareplace.ui.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.arango.miguel.shareplace.R
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        bLogin.setOnClickListener {
            v   ->

            startLoginWithPhone()

        }
    }


    private fun startLoginWithPhone(){
        val whitelistedCountries = ArrayList<String>()
        whitelistedCountries.add("+51")

        val providers = arrayListOf(
            AuthUI.IdpConfig.PhoneBuilder()
                .setWhitelistedCountries(whitelistedCountries)
                //.setDefaultNumber("+51999999999")
                .build())

        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setLogo(R.drawable.shareplace_icon)
                .build(),
            RC_SIGN_IN
        )

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val user = FirebaseAuth.getInstance().currentUser

                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {

                val builder = AlertDialog.Builder(this)
                builder.setTitle("Login failed")
                builder.setMessage("There is something wrong.")
                builder.setPositiveButton("Ok"){dialog, which ->
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()

            }
        }
    }


    companion object {
        private const val RC_SIGN_IN = 123
    }


}
