package com.arango.miguel.shareplace.ui.fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.arango.miguel.shareplace.R
import com.arango.miguel.shareplace.model.Place
import com.arango.miguel.shareplace.ui.adapter.PlaceListAdapter
import com.arango.miguel.shareplace.ui.activity.SharePlaceActivity
import com.arango.miguel.shareplace.ui.presenter.HomePresenter
import com.arango.miguel.shareplace.ui.presenter.Presenter
import com.arango.miguel.shareplace.ui.view.HomeView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_home.*



class HomeFragment : Fragment(), HomeView {


    override fun showLoadingPlaces() {
        loadingPlacesView.visibility = View.VISIBLE
    }

    override fun dismissLoadingPlaces() {
        loadingPlacesView.visibility = View.GONE
    }

    override fun renderPlaces(places: ArrayList<Place>) {
        this.places.addAll(places)
        rvPlacesFeed.adapter?.notifyDataSetChanged()
    }

    val places: ArrayList<Place> = ArrayList()
    var presenter: HomePresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPresenter()
        initRecyclerView()


        bSharePlace.setOnClickListener {
            v   ->
                val intent = Intent(activity, SharePlaceActivity::class.java)
                startActivity(intent)
        }
    }

    private fun initPresenter(){
        presenter = HomePresenter()
        presenter?.setView(this)
        presenter?.initialize()
    }

    private fun initRecyclerView(){
        rvPlacesFeed.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = PlaceListAdapter(places)
        }
    }





    companion object {
        private val TAG = "HomeFragment"
    }

}
