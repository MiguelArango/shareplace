package com.arango.miguel.shareplace.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.arango.miguel.shareplace.R
import com.arango.miguel.shareplace.ui.fragment.ProfileFragment
import com.arango.miguel.shareplace.ui.fragment.HomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {

        val selectedItemId = menuItem.itemId

        val fragment : Fragment

        when(selectedItemId){
            R.id.item_home -> {
                fragment = HomeFragment()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.containerView, fragment)
                transaction.commit()
            }

            R.id.item_profile -> {
                fragment = ProfileFragment()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.containerView, fragment)
                transaction.commit()
            }

            R.id.item_exit -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, SplashActivity::class.java)
                startActivity(intent)
                finish()

            }
        }


        return true
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bnvMenu.setOnNavigationItemSelectedListener(this)
        bnvMenu.selectedItemId = R.id.item_home
    }
}
