package com.arango.miguel.shareplace.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.airbnb.lottie.LottieAnimationView
import com.arango.miguel.shareplace.R
import com.arango.miguel.shareplace.model.Place
import com.arango.miguel.shareplace.model.UserData
import com.arango.miguel.shareplace.utils.GlideApp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import org.ocpsoft.prettytime.PrettyTime


class PlaceListAdapter(private val placeList: List<Place>) : RecyclerView.Adapter<PlaceListAdapter.PlaceViewHolder>() {




    override fun getItemCount(): Int {
        return placeList.size
    }

    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int) {
        holder.bind(placeList[position])
        holder.itemView.setOnClickListener {
                v ->

            /*
            val intent = MovieDetailActivity.newIntent(v.context, movieList[position])
            v.context.startActivity(intent)
            */
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return PlaceViewHolder(inflater, parent)

        /*
        val binding: ItemMovieBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_movie, parent, false)
        return ViewHolder(binding)
        */
    }


    class PlaceViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_place_feed, parent, false)) {
        private var tvDescription: TextView? = null
        private var tvUserName: TextView? = null
        private var civUserPhoto: ImageView? = null
        private var tvCreatedAgo: TextView? = null
        private var lavLoveIt: LottieAnimationView? = null
        private var tvHearts: TextView? = null
        private var imagesContainer: LinearLayout? = null
        private var placeView: CardView? = null
        private val mParent: ViewGroup = parent
        private var ivPlace: ImageView? = null
        var firebaseStorage = FirebaseStorage.getInstance()
        var firebaseDatabase = FirebaseFirestore.getInstance()

        var heartCounter: Int = 0

        init {
            placeView = itemView.findViewById(R.id.placeView)
            tvUserName = itemView.findViewById(R.id.tvUserName)
            tvDescription = itemView.findViewById(R.id.tvDescription)
            civUserPhoto = itemView.findViewById(R.id.civUserPhoto)
            tvCreatedAgo = itemView.findViewById(R.id.tvCreatedAgo)
            tvHearts = itemView.findViewById(R.id.tvHearts)
            imagesContainer = itemView.findViewById(R.id.imagesContainer)
            ivPlace = itemView.findViewById(R.id.ivPlace)
            lavLoveIt = itemView.findViewById(R.id.lavLoveIt)

            lavLoveIt!!.setOnClickListener { _->
                lavLoveIt!!.playAnimation()
                heartCounter++
                tvHearts?.text = heartCounter.toString()

            }
        }

        fun bind(place: Place) {
            TransitionManager.beginDelayedTransition(mParent)
            tvDescription?.text = place.description
            renderProfilePhoto(place.userUid)
            renderUserData(place.userUid)
            renderPlacesPhotos(place.photoReferences!!)
            val p = PrettyTime()
            val createdDate = place.created?.toDate()
            tvCreatedAgo?.text = p.format(createdDate)

        }

        private fun renderUserData(userUid: String){
            val userRef = firebaseDatabase.collection("users").document(userUid)
            userRef.get().addOnCompleteListener{
                    task ->
                if (task.isSuccessful) {
                    val document = task.result
                    if (document != null) {
                        val userData = document.toObject(UserData::class.java)
                        tvUserName?.text = userData?.name
                        //Log.d(ProfileFragment.TAG, "DocumentSnapshot data: " + task.result!!.data)
                    } else {
                        //Log.d(ProfileFragment.TAG, "No such document")
                    }
                } else {
                    //Log.d(ProfileFragment.TAG, "get failed with ", task.exception)
                }
            }
        }
        private fun renderPlacesPhotos(photoRefs: ArrayList<String>){
            val storageRef = firebaseStorage.reference
            for(photoRef in photoRefs){
                val imageView = ImageView(imagesContainer?.context)
                imagesContainer?.addView(imageView)
                imageView.layoutParams = LinearLayout.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT) // value is in pixels
                val placeImageReference = storageRef.child(photoRef)
                GlideApp.with(itemView.context)
                    .load(placeImageReference)
                    .into(imageView)
            }



        }
        private fun renderProfilePhoto(userUid: String){
            if(!userUid.isEmpty()){
                val storageRef = firebaseStorage.reference
                val profileRef : StorageReference? = storageRef.child("users").child(userUid).child("profile.jpg")

                GlideApp.with(itemView)
                    .load(profileRef)
                    .into(civUserPhoto!!)
            }
        }
    }



    /*
    class ViewHolder(private val binding: ItemMovieBinding):RecyclerView.ViewHolder(binding.root) {
        private val viewModel = MovieViewModel()

        /*
        fun bind(movie: Movie){
            viewModel.bind(movie)
            binding.viewModel = viewModel
        }
        */

    }
    */
}