package com.arango.miguel.shareplace.ui.fragment


import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.arango.miguel.shareplace.ui.presenter.ProfilePresenter
import com.arango.miguel.shareplace.ui.view.ProfileView
import com.arango.miguel.shareplace.utils.GlideApp
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment(), ProfileView {

    override fun renderProfilePhoto(pathReference: StorageReference) {
        GlideApp.with(this)
            .load(pathReference)
            .into(civProfilePhoto)
    }

    override fun getContext(): Context {
        return activity?.applicationContext!!
    }


    override fun setInputName(name: String) {
        tilName.editText?.setText(name)
    }

    override fun disableNameInput() {
        tilName.isEnabled = false
    }

    override fun enableNameInput() {
        tilName.isEnabled = true
    }


    var presenter: ProfilePresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.arango.miguel.shareplace.R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPresenter()


        civProfilePhoto.setOnClickListener { _ ->
                startImagePicker()
        }

        tilName.editText?.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter?.updateUserData(s.toString())
            }
        })
    }

    private fun initPresenter(){
        presenter = ProfilePresenter()
        presenter?.setView(this)
        presenter?.initialize()
    }

    private fun startImagePicker(){
        ImagePicker.cameraOnly().start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            val image : Image = ImagePicker.getFirstImageOrNull(data)
            val myBitmap = BitmapFactory.decodeFile(image.path)
            civProfilePhoto.setImageBitmap(myBitmap)
            presenter?.uploadProfilePhoto(image.path)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }


    companion object {
        private val TAG = "ProfileFragment"
    }
}
