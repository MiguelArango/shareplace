package com.arango.miguel.shareplace.ui.presenter

import android.net.Uri
import android.util.Log
import com.arango.miguel.shareplace.model.UserData
import com.arango.miguel.shareplace.ui.fragment.ProfileFragment
import com.arango.miguel.shareplace.ui.view.ProfileView
import com.arango.miguel.shareplace.utils.GlideApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File

class ProfilePresenter: Presenter<ProfileView>() {

    var firebaseDatabase = FirebaseFirestore.getInstance()
    var firebaseStorage = FirebaseStorage.getInstance()
    private lateinit var auth: FirebaseAuth
    private lateinit var currentUser: FirebaseUser


    override fun initialize() {
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser!!

        getUserData()
        getProfilePhoto()
    }

    public fun getUserData(){
        getView()?.disableNameInput()

        val userRef = firebaseDatabase.collection("users").document(currentUser.uid)
        userRef.get().addOnCompleteListener{
                task ->
            if (task.isSuccessful) {

                val document = task.result

                if (document != null) {
                    getView()?.enableNameInput()
                    val userData = document.toObject(UserData::class.java)
                    if(userData != null){
                        getView()?.setInputName(userData.name)
                    }

                } else {
                    getView()?.enableNameInput()
                }
            } else {
                getView()?.enableNameInput()
            }
        }
    }


    public fun getProfilePhoto(){
        var storageRef = firebaseStorage.reference
        var profileRef : StorageReference? = storageRef.child("users").child(currentUser.uid).child("profile.jpg")

        if(profileRef != null){
            getView()?.renderProfilePhoto(profileRef)
        }

    }


    public fun uploadProfilePhoto(path: String){
        var file = Uri.fromFile(File(path))
        var storageRef = firebaseStorage.reference
        val riversRef = storageRef.child("users").child(currentUser.uid).child("profile.jpg")
        val uploadTask = riversRef.putFile(file)
        uploadTask.addOnSuccessListener { task  ->

        }.addOnFailureListener { e  ->

        }
    }



    public fun updateUserData(name: String){
        val userPhone = currentUser.phoneNumber
        val userData =  UserData(name, userPhone!!)

        firebaseDatabase.collection("users")
            .document(currentUser.uid)
            .set(userData)

    }



}