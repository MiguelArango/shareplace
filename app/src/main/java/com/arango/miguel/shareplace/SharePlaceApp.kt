package com.arango.miguel.shareplace

import android.app.Application
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings



class SharePlaceApp: Application() {

    override fun onCreate() {
        super.onCreate()

        val firebaseFirestore = FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .build()
        firebaseFirestore.setFirestoreSettings(settings)
    }
}