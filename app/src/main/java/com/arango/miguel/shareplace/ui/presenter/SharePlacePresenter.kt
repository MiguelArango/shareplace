package com.arango.miguel.shareplace.ui.presenter

import android.net.Uri
import com.arango.miguel.shareplace.model.Place
import com.arango.miguel.shareplace.ui.view.SharePlaceView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_share_place.*
import java.io.File

class SharePlacePresenter: Presenter<SharePlaceView>() {

    var firebaseStorage = FirebaseStorage.getInstance()
    var firebaseDatabase = FirebaseFirestore.getInstance()
    private lateinit var auth: FirebaseAuth
    private lateinit var currentUser: FirebaseUser

    override fun initialize() {
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser!!
    }


    fun sharePlace(description: String, photosReferences: ArrayList<String>){

        var newPlace = Place(currentUser.uid, description, photosReferences)
        firebaseDatabase.collection(Place.COLLECTION_NAME)
            .add(newPlace).addOnCompleteListener { task ->

                getView()?.onSharePlaceSuccess()

            }.addOnFailureListener { e ->
                e.printStackTrace()
            }
    }

    fun uploadPhoto(path: String){
        var file = Uri.fromFile(File(path))

        var storageRef = firebaseStorage.reference
        val riversRef = storageRef.child("users").child(currentUser.uid).child("places").child(createUniqueCodeString())
        val uploadTask = riversRef.putFile(file)
        uploadTask.addOnSuccessListener { task  ->

            getView()?.onAddPhotoReference(task.metadata!!.path)

        }.addOnFailureListener { e  ->

        }
    }


    fun createUniqueCodeString(): String {
        val tsLong = System.currentTimeMillis() / 1000
        return java.lang.Long.toString(tsLong, 36).toUpperCase()
    }


}