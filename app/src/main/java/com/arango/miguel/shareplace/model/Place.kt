package com.arango.miguel.shareplace.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.PropertyName
import com.google.firebase.firestore.ServerTimestamp
import kotlin.collections.ArrayList

data class Place(
    val userUid: String = "",
    val description: String = "",
    val photoReferences: ArrayList<String>? = null,
    @get:PropertyName(CREATED)
    @set:PropertyName(CREATED)
    @ServerTimestamp var created: Timestamp? = null,
    @get:PropertyName(POSTED_DATE)
    @set:PropertyName(POSTED_DATE)
    var postedDate: Timestamp? = Timestamp.now()){
    companion object {
        const val COLLECTION_NAME = "places"
        const val CREATED = "created"
        const val POSTED_DATE = "posted_date"
    }
}



